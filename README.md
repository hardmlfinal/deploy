# deploy

## Gateway, faiss_worker deploy into swarm and emb_serving start up


### Applications
- **gateway**  
  1) Recieves text request 
  2) Converts it into embedding via emb_serving( USE model in tensorflow serving)
  3) Determine nearest cluster
  4) Sends POST request to faiss_worker, gets nearest neighbours (questions)
  5) Returns questions through nginx 
- **faiss_worker**
  1) Receives embedding from gateway, finds N nearest neighbours
  3) Returns them to gateway


- **emb_serving**
  - Serving USE model (https://tfhub.dev/google/universal-sentence-encoder-multilingual/3)
  - Runs separately  

### Architecture

![plot](./img/app.png)

### Example of work 

![plot](./img/demo.png)

### Lounch service

1) Run emb_serving: `ansible-playbook start_emb_serving.yml -i hosts.ini`
2) Run gateway + faiss_workers (green): `ansible-playbook deploy_main_app.yml -i hosts.ini --extra-vars @config/green.yml`
3) Run conditional update gateway + faiss_workers - same command, but green --> blue

### Update service with new data
Update via **Blue/Green Deployment**:

1) Change variable `data_gen_version` in `config/green.yml` to a updated
2) Run `ansible-playbook deploy_main_app.yml -i hosts.ini --extra-vars @config/green.yml`
3) Check the performance
4) If ok - nginx: prod -> green, staging -> blue via https://gitlab.com/hardmlfinal/nginx 

### Addition
- gateway choose faiss_worker by port number
- Data served locally on hosts 
- Models in faiss_workers better serve in Mlflow or other registry.
- Considering that we have 2 hosts and we want to allocate the load - we deploy 2 faiss_workers per host (4 clusters in total) via docker compose constraints 
- Required 2 gateways, because:
  - 2 data versions, 2 centroids
  - Staging and prod required (2 data samples )
- To update model - up another emb_serving and update it via blue/green

TG: https://t.me/matveymex

P.S. данных лежит мало на сервере, так вышло, но вроде как это не основная тема задания была, так что, надеюсь, простите